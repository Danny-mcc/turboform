@extends('TurboForm::layouts.master')
@section('content')

    <div class="turbo-form">

        <h1>Contact Us</h1>

        {!! Session::get('successMessage') !!}

        <form method="post" action="{!! route('turboFormProcess') !!}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {!! $errors->first('name', 'has-error has-feedback') !!}">
                        <label for="name">Name</label>
                        {!! $errors->first('name', '<small class="text-danger">:message</small>') !!}
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{!! old('name') !!}" />
                    </div>

                    <div class="form-group {!! $errors->first('email', 'has-error has-feedback') !!}">
                        <label for="email">Email</label>
                        {!! $errors->first('email', '<small class="text-danger">:message</small>') !!}
                        <input type="email" class="form-control" id="email" placeholder="Email Address" name="email" value="{!! old('email') !!}">
                    </div>

                    <div class="form-group {!! $errors->first('phone', 'has-error has-feedback') !!}">
                        <label for="phone">Phone Number</label>
                        {!! $errors->first('phone', '<small class="text-danger">:message</small>') !!}
                        <input type="phone" class="form-control" id="phone" placeholder="Phone Number" name="phone" value="{!! old('phone') !!}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group {!! $errors->first('message', 'has-error has-feedback') !!}">
                        <label for="name">Message</label>
                        {!! $errors->first('message', '<small class="text-danger">:message</small>') !!}
                        <textarea class="form-control" rows="8" name="message" placeholder="Message">{!! old('message') !!}</textarea>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-right">Send</button>
                </div>
            </div>

        </form>
    </div>
@endsection
