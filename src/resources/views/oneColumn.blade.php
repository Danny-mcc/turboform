@extends('TurboForm::layouts.master')
@section('content')

        <h1 class="text-center">Contact Us</h1>

        @if( Session::has('successMessage'))
            <p class="bg-primary">
                {{ Session::get('successMessage') }}
            </p>
        @endif

        <form method="post" action="{!! route('turboFormProcess') !!}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {!! $errors->first('name', 'has-error has-feedback') !!}">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{!! old('name') !!}" />
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! $errors->first('email', 'has-error has-feedback') !!}">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Email Address" name="email" value="{!! old('email') !!}">
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! $errors->first('phone', 'has-error has-feedback') !!}">
                        <label for="phone">Phone Number</label>
                        <input type="phone" class="form-control" id="phone" placeholder="Phone Number" name="phone" value="{!! old('phone') !!}">
                        {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! $errors->first('message', 'has-error has-feedback') !!}">
                        <label for="name">Message</label>
                        <textarea class="form-control" rows="4" name="message" placeholder="Message">{!! old('message') !!}</textarea>
                        {!! $errors->first('message', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-right">Send</button>
                </div>
            </div>

        </form>

@endsection
