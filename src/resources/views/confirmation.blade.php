@extends('TurboForm::layouts.master')
@section('content')
    <div class="text-center">

        <h1>Message sent.</h1>

        <p class="lead">
            Thank you for contacting us.<br>
            We will get back to you shortly.
        </p>

        <p>
            <a href="{!! route('turboForm') !!}">Send another message.</a>
        </p>

    </div>
@endsection
