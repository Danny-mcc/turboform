<?php

namespace DannyMcc\TurboForm;

use Illuminate\Support\ServiceProvider;

class TurboFormServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->bind('TurboForm', function(){
            return new \DannyMcc\TurboForm\TurboForm;
        });

        // Include Routes
        include __DIR__.'/Http/routes.php';

        // Set default path for views.
        $this->loadViewsFrom(__DIR__.'/resources/views', 'TurboForm');

        // Publish config file and migration to create table.
        $this->publishes([
            __DIR__.'/config/turboform.php' => config_path('turboform.php')
        ], 'config');

        $this->publishes([
            __DIR__.'/migrations' => database_path().'/migrations',
        ], 'migrations');

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/turboform.php', 'turboform');
    }
}