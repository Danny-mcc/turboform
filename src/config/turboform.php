<?php

return [

    // To address (Leave commented out to use your default address).
    //'to' => 'dan@rocketone.co.uk',

    // Email subject
    'subject' => 'Turbo Form',

    // Form template (Options: oneColumn, twoColumn)
    'template' => 'oneColumn',

];