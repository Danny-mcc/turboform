<?php

namespace DannyMcc\TurboForm\Facades;
/**
 * Turbo Form facade.
 */
class TurboForm extends \Illuminate\Support\Facades\Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'TurboForm';
    }
}