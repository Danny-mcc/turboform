<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurboFormTable extends Migration
{
    /**
     * Create turbo_form table for Turbo Form.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turbo_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('phone', 20);
            $table->string('email', 255);
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Drop turbo_form table
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('turbo_forms');
    }
}
