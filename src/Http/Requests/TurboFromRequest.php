<?php

namespace DannyMcc\TurboForm\Http\Requests;

use App\Http\Requests\Request;

class TurboFormRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'message' => 'required',
            'email' => 'required|email'
        ];
    }

}