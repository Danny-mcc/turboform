<?php

namespace DannyMcc\TurboForm\Http\Controllers;

use App\Http\Controllers\Controller;
use DannyMcc\TurboForm\Http\Requests\TurboFormRequest;
use DannyMcc\TurboForm\TurboForm;


class TurboFormController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Turbo Form Controller
    |--------------------------------------------------------------------------
    */

    protected $turboForm;


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(TurboForm $turboForm)
    {
        $this->turboForm = $turboForm;
    }


    /**
     * Create form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form()
    {
        return $this->turboForm->make();
    }


    /**
     * Process form.
     *
     * @param TurboFormRequest $request
     * @return mixed
     */
    public function postForm(TurboFormRequest $request)
    {

        $inputs = $request->only(['name', 'email', 'phone', 'message']);

        $this->turboForm->process($inputs);

        return redirect()->route('turboFormConfirmation')->with('successMessage', 'Message Sent.');

    }

    /**
     * Confirmation page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmation()
    {
        return view('TurboForm::confirmation');
    }
}
