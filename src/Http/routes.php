<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/form', 'DannyMcc\TurboForm\Http\Controllers\TurboFormController@form')->name('turboForm');
    Route::post('/form', 'DannyMcc\TurboForm\Http\Controllers\TurboFormController@postForm')->name('turboFormProcess');
    Route::get('/confirmation', 'DannyMcc\TurboForm\Http\Controllers\TurboFormController@confirmation')->name('turboFormConfirmation');
});
