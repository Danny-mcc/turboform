<?php

namespace DannyMcc\TurboForm;


use Illuminate\Support\Facades\Mail;
use \DannyMcc\TurboForm\Models\TurboForm as TurboFormModel;

class TurboForm
{

    public function make($template = null)
    {
        // Use template set in config by default.
        if(is_null($template)) {
            $template = config('turboform.template');
        }


        // Overwrite default template if one exists.
        if(view()->exists($template)){
            return view("$template");
        }

        return view("TurboForm::$template");

    }

    public function process($inputs)
    {
        // Save to database
        $this->saveForm($inputs);

        // Send Email
        $this->sendEmail($inputs);
    }

    private function saveForm($inputs)
    {
        TurboFormModel::create($inputs);
    }

    private function sendEmail($inputs)
    {
        Mail::send('TurboForm::email', ['inputs' => $inputs], function($message) use ($inputs){
            $message->to(config('turboform.to', config('mail.from.address')));
            $message->replyTo($inputs['email']);
            $message->subject(config('turboform.subject'));
        });
    }

}