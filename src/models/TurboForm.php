<?php

namespace DannyMcc\TurboForm\Models;

use Illuminate\Database\Eloquent\Model;

class TurboForm extends Model
{
    protected $fillable = [
        'name', 'message','phone', 'email'
    ];
}
