<?php

namespace DannyMcc\TurboForm\Test;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DannyMcc\TurboForm\TurboForm;
use Illuminate\Support\Facades\Mail;
use Mockery;
use TestCase;

class MailTest  extends TestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * A basic functional test example.
     * @test
     * @return void
    */
    public function it_sends_an_email()
    {
        $formData = [
            'name'      => 'Dan',
            'email'     => 'dan@rocketone.co.uk',
            'phone'     => '0728932823823',
            'message'   => 'Here is My message'
        ];

        Mail::shouldReceive('send')->once()
            ->andReturnUsing(function ($view, $message) use ($formData) {
                $this->assertEquals('TurboForm::email', $view);
                $this->assertEquals($formData, $message['inputs']);
            });

        $this->call('POST', '/form', $formData);
    }

}
