<?php

namespace DannyMcc\TurboForm\Test;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DannyMcc\TurboForm\TurboForm;
use TestCase;

class FormTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->turboForm = new TurboForm();
    }

    /**
     * Functional test.
     * @test
     * @return void
    */
    public function it_saves_and_shows_the_confirmation_page()
    {

        $formData = [
            'name'      => 'Dan',
            'email'     => 'dan@rocketone.co.uk',
            'phone'     => '0728932823823',
            'message'   => 'Here is My message'
        ];

        $this->visit('/form')
            ->see('Contact Us')
            ->submitForm('Send', $formData)
            ->seeInDatabase('turbo_forms', $formData)
            ->seePageIs('/confirmation')
            ->see('Message Sent.');
    }

}
