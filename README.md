# Turbo Form

Simple form package.

## Installation

Add the following to your composer.json:
    
	"repositories": [
		{
			"type": "vcs",
			"url": "https://bitbucket.org/Danny-mcc/turboform.git"
		}
	],
	"require": {
		"DannyMcc/TurboForm": "dev-master"
	}

run `composer update`

Then add the Service provider to `config/app.php`

``` php
    'providers' => [
        // ...
        DannyMcc\TurboForm\TurboFormServiceProvider::class
    ]
```

run `php artisan vendor:publish`

This will create a migration and the config file `config/turboform.php` used by Turbo Form.

run `php artisan migrate` to run migration.

## Configuration

By default Turbo Form will send an email using your laravel default email address. You can overwrite this by uncommenting or adding the following line to the `config/turboform.php` config file.

``` php
    'to' => 'your@emailaddress.com',
```

You can change the subject of the email

``` php
    'subject' => 'My Subject',
```

And select betweeen two default templates
``` php
    // Form template (Options: singleColumn, twoColumn)
    'template' => 'twoColumn',
```

## Usage

To use Turbo Form, go to `http://your.app/form`.